import React, { Component } from "react";
import Footer from "./components/page/Footer";
import Header from "./components/page/Header";
import Body from "./components/page/Body";
import ChatBot from "./components/chatbot/ChatBot";
import "./App.css";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header title={"Hi Tayo! "} />
        <Body />
        <ChatBot />
        <Footer />
      </div>
    );
  }
}
export default App;
