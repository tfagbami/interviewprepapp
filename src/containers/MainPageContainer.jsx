import React, { Component } from "react";
import Footer from "./components/page/Footer";
import Header from "./components/page/Header";
import Body from "./components/page/Body";

class MainPageContainer extends Component {
  render() {
    return (
      <div>
        <Header title={"Hi Tayo! "} />
        <Body />
        <Footer />
      </div>
    );
  }
}
export default MainPageContainer;
