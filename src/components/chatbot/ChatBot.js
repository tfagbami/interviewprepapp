import React from "react";
import ChatBot from "react-simple-chatbot";

import { ThemeProvider } from "styled-components";

function CustomChatBot(props) {
  const config = {
    width: "300px",
    height: "400px",
    floating: true
  };

  const theme = {
    background: "white",
    fontFamily: "Arial, Helvetica, sans-serif",
    headerBgColor: "#00B2B2",
    headerFontColor: "#fff",
    headerFontSize: "25px",
    botBubbleColor: "#00B2B2",
    botFontColor: "#fff",
    userBubbleColor: "#fff",
    userFontColor: "#4c4c4c"
  };

  const steps = [
    {
      id: "Greet",
      message: "Hello, Welcome to Rahul Site",
      trigger: "Ask Name"
    },
    {
      id: "Ask Name",
      message: "What is your name!!",
      trigger: "Waiting user input for name"
    },
    {
      id: "Waiting user input for name",
      user: true,
      trigger: "Services"
    },
    {
      id: "Services",
      message:
        "Hi {previousValue}, what services are you interested in System design or Algorithms!!",
      trigger: "Select Services"
    },
    {
      id: "Select Services",
      user: true,
      trigger: "Selected Service"
    },
    {
      id: "Selected Service",
      message:
        "you are interested in {previousValue}. We will be starting a {previousValue} program in 2020 !!",
      trigger: "Done"
    },
    {
      id: "Done",
      message: "Have a great day !!",
      end: true
    }
  ];

  return (
    <ThemeProvider theme={theme}>
      <ChatBot steps={steps} {...config} />
    </ThemeProvider>
  );
}
/* function ChatBot(props) {
  const steps = [
    {
      id: "Greet",
      message: "Hello, Welcome to Rahul Site",
      trigger: "Done"
    },
    {
      id: "Done",
      message: "Have a great day !!",
      end: true
    }
  ];
  //call of component
  return <ChatBot steps={steps} />;
} */

export default CustomChatBot;
