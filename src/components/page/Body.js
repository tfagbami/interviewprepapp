import React, { Component } from "react";
const Body = () => (
  <p className="App-intro">
    Are you interested in working for these companies.
    <ul>
      <li>Amazon</li>
      <li>Google</li>
      <li>Facebook</li>
      <li>Dropbox</li>
    </ul>
    We can help
  </p>
);

export default Body;
